import { useState } from "react";
import "../../../src/App.css";
import axios from "axios";

function Register() {
  const [firstName, setfName] = useState("");
  const [lastName, setlName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setphoneNumber] = useState("");
  const [password, setPassword] = useState("");

  // const handleChange = ({currentTarget.input}) => {
  //   setUser({...user, [input.name]: input.value})
  // };

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);

  const [user, setUser] = useState({
    firstName: "",
    lName: "",
    username: "",
    email: "",
    phoneNumber: "",
    password: "",
  });
  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setUser({
      ...user, //spread operator
      [name]: value,
    });
    console.log(user);
  };

  const handlefName = (e: any) => {
    setfName(e.target.value);
    setSubmitted(false);
  };
  const handlelName = (e: any) => {
    setlName(e.target.value);
    setSubmitted(false);
  };
  const handleUserName = (e: any) => {
    setUsername(e.target.value);
    setSubmitted(false);
  };
  const handleEmail = (e: any) => {
    setEmail(e.target.value);
    setSubmitted(false);
  };
  const handleMobileNo = (e: any) => {
    setphoneNumber(e.target.value);
    setSubmitted(false);
  };
  const handlePassword = (e: any) => {
    setPassword(e.target.value);
    setSubmitted(false);
  };

  //register function
  // const Register = () => {
  //   debugger;
  //   const user = { fName, lName, username, email, phoneNumber, password };
  //   console.log("user", user);
  //   if (fName && lName && username && email && phoneNumber && password) {
  //     debugger;
  //     axios
  //       .post("http://localhost:4000/api/user/register", user)
  //       .then((res: any) => console.log(res));
  //   } else {
  //     alert("invalid input");
  //   }
  // };

  const http = axios.create({ baseURL: "http://localhost:4000/" });

  const onSubmitRegister = () => {
    debugger;
    const user = {
      firstName,
      lastName,
      username,
      phoneNumber,
      email,
      password,
    };
    console.log(user);
    if (firstName && lastName && username && email && password && phoneNumber) {
      http.post("api/user/register", user).then((res) => {
        alert("User Saved Successfully");
      });
    } else {
      alert("invalid input");
    }
  };

  return (
    <div className="App">
      <h1>Register</h1>

      <form>
        <div className="container">
          <input
            type="text"
            placeholder="First Name"
            value={firstName}
            required
            onChange={handlefName}
          />
          <input
            value={lastName}
            type="text"
            className=""
            placeholder="Last Name"
            required
            onChange={handlelName}
          />
          <input
            value={username}
            type="text"
            placeholder="Username"
            required
            onChange={handleUserName}
          />
          <input
            value={phoneNumber}
            type="text"
            className=""
            placeholder="Mobile No"
            required
            onChange={handleMobileNo}
          />
          <input
            value={email}
            type="email"
            className=""
            placeholder="Email"
            required
            onChange={handleEmail}
          />
          <input
            value={password}
            type="password"
            className=""
            placeholder="Password"
            required
            onChange={handlePassword}
          />

          <button
            type="submit"
            className="registerbtn"
            onClick={onSubmitRegister}
          >
            Register
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;
